#use wml::debian::translation-check translation="0cfee114b2d0c22d16e262ece0a0804aac60d237" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В Tomcat, движке сервлетов и JSP, было обнаружено несколько уязвимостей,
которые могут приводить к подделке HTTP-запросов, выполнению кода
в AJP-коннекторе (по умолчанию отключён в Debian) или атакам по принципу
человек-в-середине на JMX-интерфейс.</p>

<p>В стабильном выпуске (buster), эти проблемы были исправлены в
версии 9.0.31-1~deb10u1. Исправление для <a href="https://security-tracker.debian.org/tracker/CVE-2020-1938">\
CVE-2020-1938</a> может потребовать изменения настроек,
если Tomcat используется с AJP-коннектором, например,
вместе с libapache-mod-jk. Например, атрибут
<q>secretRequired</q> теперь по умолчанию имеет значение true. Рекомендуется
ознакомиться с <a href="https://tomcat.apache.org/tomcat-9.0-doc/config/ajp.html">\
https://tomcat.apache.org/tomcat-9.0-doc/config/ajp.html</a>
до выполнения развёртывания обновления.</p>

<p>Рекомендуется обновить пакеты tomcat9.</p>

<p>С подробным статусом поддержки безопасности tomcat9 можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/tomcat9">\
https://security-tracker.debian.org/tracker/tomcat9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4680.data"
