#use wml::debian::template title="Debians stadgar" BARETITLE="true"
#use wml::debian::translation-check translation="fc833cc9f6e2802eb17b3bcad31adb85cb69a260" mindelta="1" maxdelta="1"
#use wml::debian::toc

<p>Detta är en översättning från det
<a href="constitution.en.html">engelska originalet</a>

<h1>Stadgar för Debianprojektet (v1.8)</h1>

<p>
Version 1.8 stadfäst den 28 augusti 2022.</p>

<p>Ersätter
<a href="constitution.1.7">Version 1.7</a> stadfäst den 14 augusti 2016.
<a href="constitution.1.6">Version 1.6</a> stadfäst den 13 December 2015.
<a href="constitution.1.5">Version 1.5</a> stadfäst den 9 januari 2015. 
<a href="constitution.1.4">Version 1.4</a> stadfäst den 7 oktober 2007.
<a href="constitution.1.3">Version 1.3</a> stadfäst den 24 september 2006,
<a href="constitution.1.2">Version 1.2</a>, stadfäst den 29 oktober 2003,
<a href="constitution.1.1">Version 1.1</a>, stadfäst den 21 juni 2003
samt
<a href="constitution.1.0">Version 1.0</a>, stadfäst den 2 december 1998.
</p>

<toc-display/>
<toc-add-entry name="item-1">§1. Introduktion</toc-add-entry>

<p><cite>Debianprojektet är en sammanslutning av individer som har gjort
gemensam sak att skapa ett fritt operativsystem.</cite></p>

<p>Detta dokument beskriver organisationsstrukturen för formella
beslut i projektet.  Det beskriver inte målen med projektet eller hur
det uppnår dem, och innehåller inte heller några riktlinjer förutom
dem som är direkt relaterade till beslutsprocessen.</p>

<toc-add-entry name="item-2">§2. Beslutande organ och individer</toc-add-entry>

<p>Varje beslut i projektet fattas av en eller flera av följande:</p>

<ol>
  <li>Utvecklarna, genom en allmän resolution eller ett val;
  </li>
  <li>Projektledaren;
  </li>
  <li>Tekniska kommittén och/eller dess ordförande;
  </li>
  <li>De individuella utvecklarna som arbetar med ett bestämt uppdrag;
  </li>
  <li>Delegater utnämnda av projektledaren för specifika uppdrag;
  </li>
  <li>Projektsekreteraren;
  </li>
</ol>

<p>Huvuddelen av resten av detta dokument kommer att sammanfatta dessa
organs befogenheter, deras sammansättning och tillsättande, och procedurerna
för deras beslutstagning.
Befogenheterna för en person eller ett organ kan vara föremål för granskning
och/eller begränsning av andra; i sådana fall kommer det granskande organets
eller personens avsnitt att visa detta.
<cite>I listan ovan listas vanligtvis personer och organ före de personer
eller organ vars beslut de kan upphäva eller dem de kan (hjälpa till att)
utnämna - men inte alla som listas tidigare kan upphäva beslut från alla som
listas senare.</cite></p>

<h3>§2.1. Generella regler</h3>

<ol>
  <li>
    Ingenting i dessa stadgar förpliktar någon att arbeta för projektet.
    En person som inte vill utföra en uppgift som delegerats eller
    tilldelats honom eller henne behöver inte utföra den.
    De får dock inte aktivt motarbeta dessa regler och
    beslut som tagits i enlighet med dem.
  </li>

  <li>
    En person kan inneha flera poster, med undantag för att projektledaren,
    projektsekreteraren och ordföranden för den tekniska kommittén måste
    vara åtskilda, och att ledaren inte kan tillsätta sig själv som sin
    egen delegat.
  </li>

  <li>
    En person kan när som helst lämna projektet eller avsäga sig den post
    han eller hon innehar genom att säga så offentligt.
  </li>
</ol>

<toc-add-entry name="item-3">§3. Individuella utvecklare</toc-add-entry>

<h3>§3.1. Befogenheter</h3>

<p>En individuell utvecklare kan</p>

<ol>
  <li>ta tekniska eller icke-tekniska beslut gällande sitt eget arbete;
  </li>
  <li>föreslå eller sekundera allmänna resolutioner;
  </li>
  <li>föreslå sig själv som projektledarkandidat i val;
  </li>
  <li>rösta på allmänna resolutioner och i val till ledare.
  </li>
</ol>

<h3>§3.2. Sammansättning och tillsättning</h3>

<ol>
  <li>
    Utvecklare är frivilliga som samtycker att främja projektets mål
    i den mån de deltar i det, och som underhåller paket för projektet,
    eller utför annat arbete som projektledarens delegat(er) anser
    givande.
  </li>

  <li>
    Projektledarens delegat(er) kan välja att inte släppa in nya utvecklare,
    eller att utesluta existerande utvecklare.
    <cite>Om utvecklarna anser att delegaterna missbrukar sina
    befogenheter kan de naturligtvis upphäva besluten genom en allmän
    resolution - se §4.1(3) och §4.2.</cite>
  </li>
</ol>

<h3>§3.3. Procedurer</h3>

<p>Utvecklarna kan ta dessa beslut efter eget godtycke.</p>

<toc-add-entry name="item-4">§4. Utvecklarna genom allmän resolution eller val</toc-add-entry>

<h3>§4.1. Befogenheter</h3>

<p>Tillsammans kan utvecklarna:</p>

<ol>
  <li>
    Tillsätta eller avsätta projektledaren.
  </li>

  <li>
    Göra tillägg till dessa stadgar, förutsatt ett 3:1-flertal.
  </li>

  <li>
    Ta eller ändra beslut som ingår i de befogenheter som givits
    projektledaren eller en av dennes delegater.
  </li>

  <li>
    Ta eller ändra beslut som ingår i de befogenheter som givits
    den tekniska kommittén, givet att enighet uppnås med en
    2:1-majoritet.
  </li>


  <li>
    Utfärda, ersätta och dra tillbaka icke-tekniska policydokument och
    uttalanden.

    <p>Till dessa räknas dokument som beskriver projektets mål, dess
    förhållande till andra organ inom fri programvara, och icke-tekniska
    riktlinjer såsom licensvillkor för fri programvara som Debianprogramvara
    måste tillmötesgå.</p>

    <p>Dessutom kan detta innehålla offentliga ställningstaganden hänseende
    aktuella ämnen.</p>

    <ol style="list-style: decimal;">
     <li>
      Ett grundläggande dokument är ett dokument eller ett
      ställningstagande som anges vara kritiskt för projektets
      arbete och ändamål.
     </li>
     <li>
      De grundläggande dokumenten är verken med
      <q>Debians sociala kontrakt</q>
      samt
      <q>Debians riktlinjer för fri programvara</q>.
     </li>
     <li>
      För att ersätta ett grundläggande dokument krävs en 3:1-majoritet.
      Nya grundläggande dokument kan ges ut och befintliga dras tillbaka
      genom att ändra i listan över grundläggande dokument i dessa
      stadgar.
     </li>
    </ol>

  <li>
    Ta beslut om egendom som förvaltas för ändamål relaterade till Debian.
    (Se §9).
  </li>

  <li>
    Vid oenighet mellan projektledaren och sekreteraren, tillsätta en
    ny sekreterare.
  </li>
</ol>

<h3>§4.2. Procedurer</h3>

<ol>
  <li>
    Utvecklarna följer de normala resolutionsprocedurerna, beskrivna nedan.
    En resolution eller  ett valalternativ introduceras om den föreslagits av
    någon av utvecklarna och sekunderats av åtminstone K andra utvecklare, eller
    om den föreslagits av projektledaren eller den tekniska kommittén.

  <li>
    Uppskjutning av beslut gjorda av projektledaren eller dennes delegat:

    <ol>
      <li>Om projektledaren, dennes delegat eller den tekniska
      kommittén har tagit ett beslut kan utvecklarna upphäva detta
      genom att framlägga en resolution om att göra detta, se §4.1(3).

      <li>Om en sådan resolution sekunderas av åtminstone 2K utvecklare,
      eller om det föreslås av den tekniska kommittén, kommer resolutionen
      omedelbart att uppskjutas (såvida resolutionen själv säger detta).

      <li>Om det ursprungliga förslaget var att ändra en diskussions-
      eller omröstningsperiod, eller om resolutionen avser att
      upphäva ett beslut av den tekniska kommittén, behöver endast
      K utvecklare sekundera resolutionen för att omedelbart kunna
      uppskjuta beslutet.

      <li>Om beslutet uppskjuts hålls en omedelbar omröstning för att
      bestämma om beslutet skall vara giltigt tills en full omröstning
      kan hållas om beslutet, eller om fullföljandet av det ursprungliga
      beslutet skall skjutas upp till den tidpunkten.
      Det finns inte något minsta beslutsmässiga antal för denna
      omedelbara proceduromröstning.

      <li>Om projektledaren (eller delegaten) drar tillbaka det ursprungliga
      beslutet blir omröstningen inaktuell och kommer inte längre
      att hållas.
    </ol>

  <li>
    Röster tas emot av projektsekreteraren.
    Röster, ställningar och röstresultat tillkännages inte under
    omröstningsperioden;
    efter omröstningsperioden tillkännager projektsekreteraren alla
    avgivna röster.
    Omröstningsperioden varar två veckor, men projektledaren kan
    lägga till eller dra ifrån upp till en vecka.

  <li>
    Projektledaren har utslagsröst.
    Minsta beslutsmässiga antalet projektdeltagare är 30. Förhandsvalet
    är "Inga av ovanstående".</li>

  <li>
    Förslag, sekunderingar, valalternativ, begäran om omröstning och andra
    formella handlingar görs via kungörelser på en allmänt tillgänglig
    elektronisk sändlista utvald av projektledarens delegat(er);
    alla utvecklare kan posta på den.

  <li>
    Röster avges via e-post på ett sätt som är ändamålsenligt för
    sekreteraren.
    Sekreteraren bestämmer för varje omröstning huruvida de röstande
    kan ändra sina röster.
  </li>

  <li>
    Q är hälften av kvadratroten av det nuvarande antalet utvecklare.
    K är det minsta av Q och 5.
    Q och K måste inte vara heltal och avrundas inte.
</ol>

<toc-add-entry name="item-5">§5. Projektledaren</toc-add-entry>

<h3>§5.1. Befogenheter</h3>

<p><a href="leader">Projektledaren</a> kan:</p>

<ol>
  <li>
    Utnämna delegater eller delegera beslut till den tekniska kommittén.

    <p>Ledaren kan definiera ett område med löpande ansvar eller
    ett särskilt beslut och överlämna det till en annan utvecklare
    eller till den tekniska kommittén.</p>

    <p>När ett enskilt beslut har delegerats och tagits kan inte
    projektledaren ta tillbaka denna delegering;
    däremot kan han eller hon ta tillbaka en löpande delegering
    för ett bestämt ansvarsområde.</p>

  <li>
    Ge fullmakt till andra utvecklare.

    <p>Projektledaren kan göra uttalanden till stöd för synpunkter
    eller andra projektmedlemmar, tillfrågad eller inte;
    dessa uttalanden är giltiga om och endast om ledaren hade haft
    befogenhet att ta beslutet ifråga.</p>

  <li>
    Ta beslut som kräver brådskande handling.

    <p>Detta gäller inte beslut som har blivit brådskande bara
    på grund av brist på relevant handling med mindre än att det
    finns en bestämd tidsfrist.</p>

  <li>
    Ta beslut för vilka inga andra har ansvar.

  <li>
    <p>Föreslå allmänna resolutioner och valalternativ till allmänna resolutioner.
    När ett valalternativ föreslås av projektledaren krävs inte
    sponsorer för den allmänna resolutionen eller det valalternaitvet; se
    &sect;4.2.1.</p>

  <li>
    Tillsammans med den tekniska kommittén tillsätta nya medlemmar
    i kommittén (se §6.2).

  <li>
    Ge utslagsröst i utvecklarnas omröstningar.

    <p>Projektledaren har också vanlig rösträtt i sådana omröstningar.</p>

  <li>
    Förändra diskussionsperioden för utvecklares omröstningar
    (se ovan).

  <li>
    Leda diskussioner bland utvecklare.

    <p>Projektledaren bör försöka att delta i diskussioner bland
    utvecklarna på ett hjälpsamt sätt som försöker fokusera diskussionen
    på de nyckelfrågor den gäller.
    Projektledaren bör inte använda ledarpositionen för att främja
    sina egna personliga synpunkter.</p>

  <li>
    I samtal med utvecklarna, ta beslut som rör egendom som förvaltas
    för ändamål relaterade till Debian. (Se §9).
    Sådana beslut kommuniceras till medlemmarna av projektledaren eller
    dennes delegat(er).
    Större användning av pengar bör föreslås och diskuteras på en sändlista
    innan medel betalas ut.
  </li>

  <li>
    Lägga till eller ta bort organisationen från listan över betrodda
    organisationer (se §9), vilka är auktoriserade att ta emot och
    hålla tillgångar för Debian.
    Den utvärdering och diskussion som leder till ett sådant beslut
    äger rum på en sändlista som avsetts ändamålet av projektledaren
    eller dennes delegat(er), där alla utvecklare kan skriva inlägg.
    Det finns en minsta diskussionsperiod på två veckor innan en
    organisation kan läggas till listan över betrodda organisationer.
  </li>
</ol>

<h3>§5.2. Utnämning</h3>

<ol>
  <li>Projektledaren väljs av utvecklarna.
  </li>

  <li>
   Valet börjar sex veckor innan ledarposten frånträds, eller
  (om det redan är för sent) omedelbart.
  </li>

  <li>
   Under den följande veckan kan alla utvecklare nominera
   sig själva som projektledarkandidat och summera sina planer för
   mandatperioden.
  </li>

  <li>
   I de tre följande veckorna nomineras inga fler kandidater; kandidaterna
   bör använda denna tid för att driva kampanj och diskutera. Om det inte
   finns några kandidater vid slutet av nomineringsperioden förlängs den
   med ytterligare en vecka, om nödvändigt upprepade gånger.
  </li>

  <li>
   De två följande veckorna är valperioden, under vilken utvecklarna
  kan avge sina röster.
  Rösterna i ledarskapsval är hemliga, även efter att omröstningen
  avslutats.
  </li>

  <li>Röstsedeln skall innehålla de kandidater som nominerat sig själva
  och ännu inte dragit sig tillbaka, samt <q>Ingen av ovanstående</q>.
  Om <q>Ingen av ovanstående</q> vinner valet skall valproceduren göras om,
  flera gånger om nödvändigt.
  </li>

  <li>
   Beslutet tas med den metod som anges i $sect;A.5 i Normal resolutionsprocedur.
   Det minsta beslutsmässiga antalet är detsamma som för en allmän
   resolution (§4.2), och förhandsvalet är <q>Ingen av ovanstående</q>.
  </li>

  <li>Mandatperioden för projektledaren är ett år.
</ol>

<h3>§5.3. Procedurer</h3>

<p>Projektledaren bör försöka ta beslut som överensstämmer med konsensus 
bland utvecklarna.</p>

<p>När det är praktiskt skall projektledaren på ett informellt sätt be om
utvecklarnas synpunkter.</p>

<p>Projektledaren bör undgå att lägga för mycket vikt på sin egen
synpunkt när beslut tas i egenskap av ledare.</p>

<toc-add-entry name="item-6">§6. Teknisk kommitté</toc-add-entry>

<h3>§6.1. Befogenheter</h3>

<p>Den <a href="tech-ctte">tekniska kommittén</a> kan:</p>

<ol>
  <li>
    Ta beslut vad gäller tekniska riktlinjer.

    <p>Detta inkluderar innehållet i de tekniska policymanualerna,
    utvecklarreferensmaterialet, exempelpaket och beteendet hos de
    icke-experimentella pakettillverkningsverktygen.
    (Vid varje tillfälle tar den som underhåller programvaran eller
    dokumentationen i fråga det ursprungliga avgörandet; se §6.3(5)).</p>

  <li>
    Avgöra tekniska frågor i vilka utvecklares beslutsmandat överlappar.

    <p>I tillfällen när utvecklarna behöver implementera kompatibla
    tekniska riktlinjer eller ställningstaganden (exempelvis om de är oeniga
    om hur paket i konflikt skall prioriteras, eller om vem som äger ett
    kommandonamn, eller om vem som är ansvarig för ett programfel,
    eller om vem som skall underhålla ett paket) kan den tekniska
    kommittén avgöra saken.</p>

  <li>
    Ta beslut när de får en förfrågan om att göra så.

    <p>Alla personer och organ kan delegera egna beslut till den
    tekniska kommittén, eller be om råd från den.</p>

  <li>
    Upphäva en utvecklares beslut (kräver ett 3:1-flertal).

    <p>Den tekniska kommittén kan be en utvecklare att gå tillväga på
    ett visst sätt även om utvecklaren inte så önskar; detta kräver
    ett 3:1-flertal.
    Till exempel kan kommittén bestämma att ett klagomål framfört i
    en felrapport är rättfärdigat, och att hemställarens föreslagna
    lösning bör verkställas.</p>

  <li>
    Ge råd.

    <p>Den tekniska kommittén kan ge formella kungörelser om sin syn på
    en sak.
    <cite>Individuella medlemmar kan förstås göra informella
    ställningstaganden om sina synpunkter på kommitténs troliga
    synpunkt.</cite></p>

  <li>
    Tillsammans med projektledaren tillsätta nya medlemmar till sig
    själv eller avsätta existerande medlemmar. (Se §6.2).

  <li>
    Tillsätta den tekniska kommitténs ordförande.

    <p>Ordföranden väljs av kommittén från sina medlemmar.
    Alla medlemmar i kommittén är automatiskt nominerade;
    kommittéomröstningen startar en vecka innan posten frånträds
    (eller omedelbart om det redan är för sent).
    Medlemmarna kan rösta genom offentlig acklamation för vilken
    kommittémedlem som helst, inklusive sig själv; det finns inget
    förhandsval.
    Omröstningen avslutas när alla medlemmar har röstat, eller när
    omröstningsperioden är slut.
    Resultatet bestäms enligt den metod som anges i §A.5 av
    Normal resolutionsprocedur. Det finns ingen utslagsröst. Om det
    finns flera alternativ som inte har några nederlag i Schwartzmängden
    vid slutet av &sect;A.5.8, så kommer vinnaren att väljas slumpmässigt
    från dessa alternativ, via en mekanism som valts av projektsekreteraren.
    </p>

  <li>
    Ordförande kan tillsammans med sekreteraren vikariera för ledaren.

    <p>Såsom beskrivs i §7.1(2) kan den tekniska kommitténs ordförande
    och projektsekreteraren tillsammans vikariera för projektledaren om
    det inte finns någon ledare.
</ol>

<h3>§6.2. Sammansättning</h3>

<ol>
  <li>
    Den tekniska kommittén består av upp till åtta utvecklare, och
    bör vanligtvis ha minst fyra medlemmar.

  <li>
    Om det finns mindre än åtta medlemmar kan den tekniska kommittén
    rekommendera ny(a) medlem(mar) till projektledaren, som kan välja
    att utnämna den/dem (var och en) eller inte.

  <li>
    När det finns fem eller färre medlemmar kan den tekniska kommittén
    tillsätta ny(a) medlem(mar) tills antalet medlemmar når sex.

  <li>
    När det har varit fem eller färre medlemmar under minst en vecka kan
    projektledaren tillsätta ny(a) medlem(mar) tills antalet medlemmar når
    sex, med minst en veckas mellanrum mellan varje tillsättning.

  <li>
    En utvecklare är inte berättigad att (åter)utnämnas till tekniska
    kommittén om de har varit medlem under de föregående 12 månaderna.
    
  <li>
    Om den tekniska kommittén och projektledaren samtycker kan de ta bort
    eller ersätta existerande medlemmar från den tekniska kommittén.

  <li>
    <p>Tidsbegränsning</p>
    <ol>
      <li>
        <p>Den 1:a januari varje år sätts medlemskapet för varje
        kommittémedlem som har tjänat mer än 42 månader (3.5 år) och är
        en av de två med längst tjänstgöringstid att löpa ut den 31 december
        samma år.</p>
      </li>
      <li>
         <p>En medlem av tekniska kommittén beskrivs att ha längre
         tjänstgöringstid om de tillsattes tidigare, eller tillsattes
         vid samma tidpunkt och har varit medlem av Debianprojektet längre.
         Om en medlem har tillsatts mer än en gång så är endast den senaste
         utnämningen relevant.</p>
      </li>
    </ol>
   </li>
</ol>

<h3>§6.3. Procedurer</h3>

<ol>
  <li>
    <p>Resolutionsprocess.</p>
    
    <p>Den tekniska kommittén använder följande process för att förbereda
    en resolution för omröstning:</p>

    <ol>
      <li>Alla medlemmar av tekniska kommittén kan föreslå en resolution.
      Detta skapar en initial valsedel med två alternativ, där det andra
      alternativet är standardalternativet "ingen av ovanstående".
      Förslagsställaren för resolutionen blir förslagsställaren av
      alternativet.</li>
      
      <li>Alla medlemmar i tekniska kommittén kan föreslå ytterligare
      valsedelsalternativ eller ändra eller återkalla ett valalternativ som
      dom har föreslagit.</li>
      
      <li>Om alla valalternativ utom standardalternativet återkallas
      avbryts processen.</li>
      
      <li>Alla medlemmar i tekniska kommittén kan påkalla omröstning
      på omröstningens aktuella alternativ. Omröstningen börjar omedelbart,
      men om någon annan medlem i tekniska kommittén invänder mot att starta
      omröstningen innan omröstningen avslutas, så avbryts omröstningen och
      har ingen effekt.</li>
      
      <li>Två veckor efter det ursprungliga förslaget till valalternativen
      stängs de för ytterligare ändringar och omröstningen börjar omedelbart.
      Denna omröstning kan inte avbrytas.</li>
      
      <li>Om en omröstning avbryts under &sect;6.3.1.4 senare än 13 dagar
      efter den ursprungliga föreslagna resolutionen, börjar omröstningen
      som specificerats i &sect;6.3.1.5 istället 24 timmar efter tiden
      för avbrott. Under denna 24-timmarsperiod kan ingen efterfråga
      en omröstning, men medlemmar i tekniska kommittén kan göra ändringar
      i valalternativen enligt &sect;6.3.1.2.</li>

    </ol>
   </li>

  <li>
    <p>Detaljer rörande röstning.</p>

    <p>Röster bestäms av rösträkningsmekanismen som beskrivs i
    &sect;A.5. Omröstningsperioden är en vecka eller till resultatet
    inte längre är i tvivel om vi antar att inga medlemmar ändrar sina
    röster, vilket som är kortast. Medlemmar kan ändra sina röster ända tills
    omröstningsperioden är slut. Minsta beslutsmässiga antalet är 2.
    Ordföranden har utslagsröst. Standardalternativ är "Ingen av ovanstående".</p>
    
    <p>När tekniska kommittén röstar om att åsidosätta en utvecklare som också
    är medlem i kommittén så får inte den medlemmen rösta (om dom inte är
    ordförande, då dom endast får använda sin utslagsröst).</p>

  <li>
    Offentliga diskussioner och beslut.

    <p>Diskussioner, utkast till förslag och alternativ, och röster
    givna av kommitténs medlemmar offentliggörs på den tekniska kommitténs
    offentliga sändlista.
    Kommittén har ingen separat sekreterare.</p>

  <li>
    Förtrolighet för utnämningar.

    <p>Den tekniska kommittén kan hålla konfidentiella diskussioner via
    privat e-post eller en privat sändlista eller någon annan metod för
    att diskutera utnämningar till kommittén.
    Omröstningar angående utnämningarna måste dock göras offentliga.</p>

  <li>
    Inget detaljerat utvecklingsarbete.

    <p>Den tekniska kommittén driver inte utveckling av nya förslag
    eller riktlinjer.
    Sådant utvecklingsarbete bör göras av privatpersoner eller grupper och
    diskuteras i vanliga fora för tekniska riktlinjer eller formgivning.</p>

    <p>Den tekniska kommittén begränsar sig själv till att välja från
    eller anta kompromisser mellan lösningar och beslut som har
    föreslagits och diskuterats rimligt genomgående på annan plats.</p>

    <p><cite>Medlemmarna av den tekniska kommittén kan naturligtvis
    själva delta å egna vägar i alla aspekter av arbetet med formgivning
    och riktlinjer.</cite></p>

  <li>
    Tekniska kommittén tar beslut bara som sista utväg.

    <p>Den tekniska kommittén tar inte tekniska beslut förrän försök att
    lösa det genom konsensus har försökts och misslyckats, såvida den
    inte har ombetts att ta ett beslut av den person eller organ som
    vanligtvis skulle haft ansvar för att ta det.</p></li>
    
   <li>
      <p>Föreslå en allmän resolution.</p>
      <p>När den tekniska kommittén föreslår en allmän resolution eller ett
      valalternativ i en allmän resolution till projektet under
      &sect;4.2.1, så kan den delegera (via resolution eller andra sätt som
      överenskommits i tekniska kommittén) befogenhet att dra tillbaka, ändra
      eller göra mindre ändringar till valalternativet till en av sina
      medlemmar. Om den inte gör detta så måste dessa beslut göras genom
      en resolution i tekniska kommittén.</p>
   </li>
</ol>

<toc-add-entry name="item-7">§7. Projektsekreteraren</toc-add-entry>

<h3>§7.1. Befogenheter</h3>

<p><a href="secretary">Sekreteraren</a>:</p>

<ol>
  <li>
    Samlar röster från utvecklare, och bestämmer hur många utvecklare
    det finns och vilka de är, när detta krävs enligt stadgarna.

  <li>
    Kan tillsammans med den tekniska kommitténs ordförande vikariera
    för ledaren.

    <p>Om det inte finns någon projektledare kan tekniska kommitténs
    ordförande och projektsekreteraren via ömsesidig överenskommelse ta
    beslut om de anser det vara absolut nödvändigt att göra så.</p>

  <li>
    Dömer i strider om stadgarnas tolkning.

  <li>
    Kan när som helst delegera delar av eller alla sina maktbefogenheter
    till någon annan, eller dra tillbaka en sådan delegation.
</ol>

<h3>§7.2. Utnämning</h3>

<p>Projektsekreteraren utnämns av projektledaren och den nuvarande
projektsekreteraren.</p>

<p>Om projektledaren och den nuvarande projektsekreteraren inte kan komma överens
om en ny utnämning måste de be utvecklarna att genom en allmän resolution
utnämna en ny
sekreterare.</p>

<p>Om det inte finns någon projektsekreterare eller om den nuvarande
sekreteraren är otillgänglig och inte har delegerat befogenheter för ett
beslut kan valet göras, eller delegeras av, ordförande i den tekniska
kommittén såsom tjänstgörande sekreterare.</p>

<p>Projektsekreterarens mandatperiod är ett år, varefter en ny
(åter)utnämning är nödvändig.</p>

<h3>§7.3. Procedurer</h3>

<p>Projektsekreteraren bör fatta beslut som är rättvisa och skäliga,
och helst överensstämmande med utvecklarnas konsensus.</p>

<p>När den tekniska kommitténs ordförande och projektsekreteraren vikarierar
för en frånvarande projektledare bör de bara ta beslut som är absolut
nödvändiga, och endast när de överensstämmer med utvecklarnas
konsensus.</p>

<toc-add-entry name="item-8">§8. Projektledarens delegater</toc-add-entry>

<h3>§8.1. Befogenheter</h3>

<p>Projektledarens delegater:</p>

<ol>
  <li>har de befogenheter de har delegerats till dem av projektledaren;

  <li>kan ta vissa beslut ledaren inte kan ta direkt, inkluderande
  att godta eller utesluta utvecklare eller tillsätta personer som
  inte underhåller paket som utvecklare.
  <cite>Detta är för att undvika maktkoncentration hos projektledaren,
  i synnerhet över utvecklarmedlemskap.</cite>
</ol>

<h3>§8.2. Utnämning</h3>

<p>Delegaterna utnämns av projektledaren och kan ersättas av ledaren enligt
dennes omdöme.
Projektledaren kan inte göra positionen som delegat beroende på ett
specifikt beslut taget av delegaten, och projektledaren kan inte heller
upphäva ett beslut taget av en delegat när det väl är taget.</p> 

<h3>§8.3. Procedurer</h3>

<p>Delegater kan ta beslut så som de anser lämpligt, men bör försöka
genomföra bra tekniska beslut och/eller följa konsensus.</p>

<toc-add-entry name="item-9">§9. Tillgångar som förvaltas för Debian</toc-add-entry>

<p>
I de flesta jurisdiktioner i världen kan inte Debian själv hålla pengar
eller andra tillgångar.
Tillgångar måste därför ägas av en av de olika organisationer som beskrivs i
§9.2.
</p>

<p>
Traditionellt har SPI varit den enda organisation som auktoriserats att
hålla tillgångar och pengar för Debianprojektet.
SPI skapades i Amerikas förenta stater för att förvalta pengar där.
</p>

<p>
<a href="https://www.spi-inc.org/">SPI</a>
(Software in the Public Interest,
&rdquo;Programvara för allmänhetens bästa&rdquo;)
och Debian är olika organisationer med vissa gemensamma
målsättningar.
Debian är tacksamma för det juridiska ramverk som SPI tillhandahåller.
</p>

<h3>§9.1. Förhållande till associerade organisationer</h3>

<ol>
  <li>Debians utvecklare är inte representanter för eller anställda av
  organisationer som förvaltar tillgångar för Debian, av varandra
  eller av personer med bestämmanderätt i Debianprojektet enbart genom
  att vara Debianutvecklare.
  En person som agerar som utvecklare gör så som en individ å sina egna
  vägnar.
  Vissa organisationer kan, på eget bevåg, etablera relationer med
  individer som också är Debianutvecklare.
  </li>
</ol>

<h3>§9.2. Befogenheter</h3>

<ol>
  <li>En organisation som förvaltar tilgångar för Debian har
  ingen bestämmanderätt över Debians tekniska eller icke-tekniska
  beslut, förutom att inget beslut av Debian som angår egendom som
  förvaltas av organisationern skall kräva den att agera utom sina
  juridiska befogenheter.
  </li>

  <li>Debian har inga krav på bestämmanderätt över en organisation
  som förvaltar tillgångar för Debian utöver användningen av den
  egendom som förvaltas för Debian.
  </li>
</ol>

<h3>§9.3. Betrodda organisationer</h3>

<p>
Alla donationer till Debianprojektet måste göras via en av de organisationer
som utnämnts av projektledaren (eller en delegat) till att vara auktoriserad
att förvalta tillgångar som skall användas i Debianprojektet.
</p>

<p>
Organisationer som förvaltar tillgångar för Debian bör förplikta sig vid
rimliga åtaganden för hanteringen av sådana tillgångar.
</p>

<p>
Debian har en öppet tillgänglig lista över betrodda organisationer som kan
ta emot donationer och förvalta tillgångar för Debian (däribland både
materiella och immateriella tillgångar), vilken även beskriver vilka
förpliktelser dessa organistaioner har gjorts för hur dessa tillgångar skall
förvaltas.
</p>

<toc-add-entry name="item-A">Bilaga A. Normal resolutionsprocedur</toc-add-entry>

<p>Dessa regler gäller de gemensamma beslut som tas av kommittéer och
referendum enligt ovan.</p>

<h3>A.0. Förslag</h3>

<ol>
   <li>Den formella proceduren börjar när ett utkast till resolution föreslås
   och sekunderas, såsom specificeras i &sect;4.2.1.</li>
   
   <li>Detta utkast till resolution blir ett omröstningsalternativ i en
   initial tvåalternativsomröstning, där det andra alternativet är
   standardalternativet, och förslagsställaren av utkastresolutionen blir
   förslagsställare för detta omröstningsalternativ.</li>
</ol>

<h3>A.1. Diskussioner och ändringar</h3>

<ol>
   <li>Diskussionsperioden börjar när en utkastresolution föreslås och
   sponsras. Minsta diskussionsperiod är 2 veckor. Maximal diskussionsperiod
   är 3 veckor.</li>
   
   <li>Ett nytt valsedelsalternativ kan föreslås och sponsras enligt kraven
   för en ny resolution.</li>
   
   <li>Den som föreslår ett valsedelsalternativ kan lägga till till detta
   alternativ under förutsättning att ingen av sponsorerna av valalternativet 
   vid tidpunkten för tillägget inte instämmer inom 24 timmar. Om någon av
   dom inte instämmer, förblir valalternativet oförändrat.</li>
   
   <li>Tillägg av ett valalternativ eller ändring via ett tillägg av ett
   valalternativ ändrar slutet på diskussionsperioden till att bli en
   vecka från att denna ändring gjordes, om inte detta skulle göra den totala
   dikussionsperioden kortare än minimum diskussionsperiod eller längre
   än maximal diskussionsperiod. I det senare fallet sätts istället längden
   av diskussionsperiod till maximal längd på diskussionsperioden.</li>
   
   <li>Den som föreslår ett valalternativ kan göra mindre ändringar till detta
   alternativ (exempelvis typografiska korrigeringar, korrigeringar av
   inkonsekvenser eller andra förändringar som inte ändrar innebörden),
   så vida inte någon utvecklare har invändningar inom 24 timmar. I detta fall
   ändras inte längden på diskussionsperioden. Om en utvecklare invänder så
   måste istället förändringen göras via rättelse enligt &sect;A.1.3.</li>
   
   <li>Projektledaren kan, när som helst i processen, öka eller minska
   minsta och maximala diskussionsperioden med upp till 1 vecka från deras
   ursprungliga värden i &sect;A.1.1, förutom att dom inte kan göra det på
   ett sådant sätt att det orsakar diskussionsperioden att avslutas inom
   48 timmar från att ändringen görs. Längden på diskussionsperioden
   räknas i detta fall om så som att den nya och gamla minimala och maximala
   längderna har varit där under alla tidigare ändringar under 
   &sect;A.1.1 och &sect;A.1.4.</li>
   
   <li>Standardalternativet har ingen som föreslår det och ingen sponsor, och
   kan inte ändras eller dras tillbaka.</li>
</ol>

<h3>A.2. Återkalla valalternativ</h3>

<ol>
   <li>Den som föreslår ett valalternativ kan dra sig ur. Om dom gör detta
   kan nya personer som föreslår samma valalternativ för att hålla det
   levande, och i detta fall blir den första som gör det den nya
   förslagsställaren och andra kan bli sponsorer för förslaget om det inte redan
   finns sponsorer. Alla nya förslagsställare eller sponsorer måste möta
   samma krav som för att göra ett nytt förslag eller en ny resolution.</li>
   
   <li>En sponsor av ett valalternativ kan dra sig ur.</li>
   
   <li>Om förslagsställarens och/eller sponsorernas tillbakadragande innebär
   att ett valalternativ inte har någon förslagsställare eller inte tillräckligt
   många sponsorer för att uppfylla kraven för en ny resolution, och det går
   24 timmar utan att detta åtgärdas genom att en annan förslagsställare
   och/eller sponsor kliver fram tas det bort från valutkastet. Detta förändrar
   inte längden på diskussionsperioden.</li>
   
   <li>Om alla valalternativ utom standardalternativet dras tillbaka, avbryts
   resolutionen och kommer inte att röstas om.</li>
 </ol>
 
<h3>A.3. Uppmana till omröstning</h3>

<ol>
   <li>Efter att diskussionsperioden har avslutats kommer projektsekreteraren
   att publcera röstsedeln och kalla till omröstning. Projektsekreteraren
   kan göra detta omedelbart efter slutet av diskussionsperioden och måste
   göra det inom sju dagar efter diskussionsperiodens slut.</li>
   
   <li>Projektsekreteraren bestämmer ordningen på valalternativ och dess
   sammanfattningar som använts för omröstningen. Projektsekreteraren kan
   fråga förslagsställare för valalternativ om att utarbeta dessa
   sammanfattningar och kan revidera dem för klarhet för eget godtycke.</li>

   <li>Mindre ändringar till valalternativ under &sect;A.1.5 kan endast göras
   om minst 24 timmar återstår av diskussionsperioden, eller om
   projektsekreteraren håller med om att ändringen inte förändrar innebörden
   av omröstningens alternativ och (om den skulle göra det) motiverar en
   försening av omröstningen. Projektsekreteraren kommer att tillåta 24 timmar
   för invändningar efter en sådan förändring innan uppmaningen till omröstning
   utfärdas.</li>
   
   <li>Inga nya valalternativ får föreslås, inga valalternativ
   ändras, och inga förslagsställare eller sponsorer får dra sig ur när mindre
   än 24 timmar kvarstår av diskussionsperioden, om inte denna åtgärd förlänger
   diskussionsperioden under &sect;A.1.4 med minst 24 ytterligare
   timmar.</li>
   
   <li>Åtgärder för att bevara den befintliga omröstningen kan vidtas inom de 
   sista 24 timmarna av diskussionsperioden, nämligen då en sponsor som
   invänder mot en ändring under &sect;A.1.3, en utvecklare som invänder mot
   en mindre ändring under &sect;A.1.5, som går fram som förslagsställare
   för ett befintligt valalternativ vars ursprungliga förslagsförställare
   har dragit tillbaka det enligt &sect;A.2.1 eller sponsrar ett befintligt
   valalternativ som har färre än det erforderliga antalet sponsorer på grund
   av att en sponsor har dragit sig ur under punkt &sect;A.2.2.</li>
   
   <li>Projektsekreteraren kan göra undantag från &sect;A.3.4 och acceptera
   ändringar av röstsedeln efter det att de inte längre är tillåtna, förutsatt
   att detta görs minst 24 timmar innan kallelse utfärdas för omröstning. Alla
   andra krav för att göra en ändring av valsedeln måste fortfarande uppfyllas.
   Detta förväntas vara sällsynt och bör endast göras om projektsekreteraren
   anser att det skulle vara skadligt för projektets bästa om ändringen inte
   görs.</li>
</ol>
  
<h3>A.4. Omröstningsprocedur</h3>

<ol>
  <li>Alternativ som inte har ett uttryckligt krav på övermajoritet har ett
  majoritetskrav på 1:1. Standardalternativet har inga krav på övermajoritet.</li>

  <li>Rösterna räknas enligt reglerna i &sect;A.5.</li>
 
   <li>I tveksamma fall ska projektsekreteraren besluta i ärenden rörande
   förfarandet.</li>
</ol>


<h3>A.5. Rösträkning</h3>

<ol>
  <li>
   Varje röstandes röstsedel rangordnar de alternativ omröstningen
   omfattar.
   Inte alla alternativ behöver rangordnas.
   Rangordnade alternativ anses föredras framför samtliga ej
   rangordnade alternativ.
   De röstande kan ge samma rangordning till flera alternativ.
   Alternativ utan rangordning anses inbördes ha samma rangordning.
   Detaljer om hur röstsedlarna skall fyllas i medföljer röstsedlarna.
# FIXME
# Eg: "...will be includede in the Call For Votes"
  </li>

  <li>
   Om omröstningen har ett krav R på minsta beslutsmässiga antal
   kommer eventuella alternativ utöver förhandsvalet som inte får
   åtminstone R röster som rangordnar det framför förhandsvalet
   att bortses från.
  </li>

  <li>
   Alla alternativ (förutom förhandsvalet) som inte vinner över
   förhandsvalet med den majoritet som krävs bortses från.

   <ol>
    <li>
     Givet två alternativ A och B, är V(A,B) antalet röstande
     som föredrar alternativ A framför alternativ B.
    </li>

    <li>
     Ett alternativ A vinner över förhandsvalet D med en proportionell
     majoritet på N om V(A,D) är strikt större än eller lika med
     N × V(D,A) och V(A,D) är strikt större än V(D,A).
    </li>

    <li>
     Om en supermajoritet S:1 krävs för A, är dess proportionella
     majoritet S; i andra fall är dess proportionella majoritet 1.
    </li>
   </ol>
  </li>

  <li>
   Från listan över kvarvarande alternativ genereras en lista parvis
   över vilka alternativ som vinner över andra.

   <ol>
    <li>
     Ett alternativ A vinner över alternativ B om V(A,B) är strikt
     större än V(B,A).
    </li>
   </ol>
  </li>

  <li>
   Från listan över [kvarvarande] parvisa vinster genereras en lista
   över transitiva vinster.

   <ol>
    <li>
     Ett alternativ A vinner transitivt över ett alternativ C om A
     vinner över C eller om det finns något annat alternativ B där
     A vinner över B SAMT B transitivt vinner över C.
    </li>
   </ol>
  </li>

  <li>
   Schwartzmängden konstrueras från uppsättningen transitiva vinster.

   <ol>
    <li>
     Ett alternativ A är i Schwartzmängden om det för alla alternativ B
     gäller att antingen A transitivt vinner över B, eller B inte
     transitivt vinner över A.
    </li>
   </ol>
  </li>

  <li>
   Om det finns vinster mellan alternativ i Schwartzmängden bortses
   det svagaste av sådana vinster från i listan över parvisa vinster,
   varpå vi återgår till steg 5.

   <ol>
    <li>
     En vinst (A,X) är svagare än en vinst (B,Y) om V(A,X) är mindre
     än V(B,Y).
     Dessutom gäller att (A,X) är svagare än (B,Y) om V(A,X) är lika
     med V(B,Y) och V(X,A) är större än V(Y,B).
    </li>

    <li>
     En svagaste vinst är en vinst som inte har någon vinst svagare
     än sig.
     Det kan finnas mer än en sådan vinst.
    </li>
   </ol>
  </li>

  <li>
   Om det inte finns några vinster inom Schwartzmängden väljs vinnaren
   från alternativen i Schwartzmängden.
   Om det endast finns ett sådant alternativ, vinner det.
   Om det finns flera alternativ, väljer den väljare som håller
   utslagsrösten vilket av dessa alternativ som vinner.
  </li>
</ol>

<p>
 <strong>Observera:</strong>
 Alternativ som väljarna rangordnar framför förhandsvalet är alternativ
 de finner godtagbara.
 Alternativ som rangordnas efter förhandsvalet är alternativ de finner
 icke godtagbara.
</p>

<p><cite>När de normala resolutionsprocedurerna används måste texten som
hänvisar till dem ange vad som är tillräckligt för att få ett utkast
föreslaget och/eller sekunderat, vad som är den minsta diskussionsperioden
och hur lång omröstningsperioden är.
Den måste också ange huruvida ett kvalificerat flertal krävs och/eller om
ett minsta beslutsmässiga antal (och förvalt alternativ) skall
användas.</cite></p>

<toc-add-entry name="item-B">Bilaga B. Bruk av språk och typografi</toc-add-entry>

<p>Presens indikativ (till exempel <q>är</q>) menar att ett uttalande är en regel
i dessa stadgar.
<q>Kan</q> indikerar att personen eller organet kan använda sitt omdöme.
<q>Bör</q> indikerar att det anses vara bra om meningen efterföljs, men den är
inte bindande.
<cite>Text markerad som ett citat, såsom denna, är bakgrundsmaterial och
utgör inte en del av stadgarna.
Den används enbart för att hjälpa med uttolkningen om tvekan uppkommer.
</cite></p>
