# From: r.meier@siemens.com
# Webpage URL is usually redirected, this is normal, Roger Meier 2005-09-20
#use wml::debian::translation-check translation="7dddaeaa5c20d8b39e4a465987ff2f4526a84517"

<define-tag pagetitle>Siemens</define-tag>
<define-tag webpage>https://www.siemens.com/</define-tag>

#use wml::debian::users

<p>
  Siemens använder Debian i flera områden inklusive forskning och utveckling,
  infrastruktur, produkter och lösningar.
</p>
<p>
  Debian används som basen i många av våra produkter och tjänster inom
  automatisering och digitalisering av process- och tillverkningsindustrier,
  smarta mobila lösningar för väg och räls, digitala sjukvårdstjänster och
  medicinsk teknologi så väl som distribuerade energisystem och intelligent
  infrastruktur för byggnader.
</p>
<p>
  Debian är exempelvis inbäddat och en del av
  <a href="https://www.siemens-healthineers.com/magnetic-resonance-imaging">Siemens MRI-skannrar</a>,
  ombord på moderna Siemens-tåg och tunnelbanesystem, runt
  <a href="https://press.siemens.com/global/en/feature/siemens-digitalize-norwegian-railway-network">spår och tågstationer</a>,
  och många flera av våra enheter. Dessutom är Siemens en av grundarna av
  <a href="https://www.cip-project.org/">Civil Infrastructure Platform</a>,
  som använder Debian LTS som sitt baslager.
</p>
<p>    
  Det säger sig självt att Debian också är en del av vår serverinfrastruktur.
  Våra arbetsstationer och stationära datorer kör ofta Debian när Linux
  krävs, t.ex. är de flesta Linuxbaserade produkterna i byggnadens
  teknologidomän utvecklade på Debian. Siemens erbjuder också Debianbaserade
  inbäddade distributioner, så som
  <a href="https://siemens.com/embedded">Sokol™ Omni OS</a>.
</p>
<p>    
  Varför vi håller oss till Debian: Den höga kvalitet och stabilitet som vi
  når genom att använda Debian är fantastisk! och tillgängligheten för
  mjukvaruuppdateringar och säkerhetsrättningar är enastående och mängden
  tillgängliga mjukvarupaket är fantastisk.
</p>

