msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr ""

#: ../../english/index.def:12
msgid "DebConf is underway!"
msgstr ""

#: ../../english/index.def:15
msgid "DebConf Logo"
msgstr ""

#: ../../english/index.def:19
msgid "DC22 Group Photo"
msgstr ""

#: ../../english/index.def:22
msgid "DebConf22 Group Photo"
msgstr ""

#: ../../english/index.def:26
msgid "Mini DebConf Regensburg 2021"
msgstr ""

#: ../../english/index.def:29
msgid "Group photo of the MiniDebConf in Regensburg 2021"
msgstr ""

#: ../../english/index.def:33
msgid "Screenshot Calamares Installer"
msgstr ""

#: ../../english/index.def:36
msgid "Screenshot from the Calamares installer"
msgstr ""

#: ../../english/index.def:40
#: ../../english/index.def:43
msgid "Debian is like a Swiss Army Knife"
msgstr ""

#: ../../english/index.def:47
msgid "People have fun with Debian"
msgstr ""

#: ../../english/index.def:50
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr ""

#: ../../english/template/debian/navbar.wml:31
msgid "Bits from Debian"
msgstr ""

#: ../../english/template/debian/navbar.wml:31
msgid "Blog"
msgstr ""

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews from Debian"
msgstr ""

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews"
msgstr ""

#: ../../english/template/debian/navbar.wml:33
msgid "The Planet of Debian"
msgstr ""

#: ../../english/template/debian/navbar.wml:33
msgid "Planet"
msgstr ""

