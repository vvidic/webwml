<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in qemu, a fast processor
emulator. The Common Vulnerabilities and Exposures project identifies
the following problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9603">CVE-2016-9603</a>

    <p>qemu-kvm built with the Cirrus CLGD 54xx VGA Emulator and the VNC
    display driver support is vulnerable to a heap buffer overflow
    issue.  It could occur when Vnc client attempts to update its
    display after a vga operation is performed by a guest.</p>

    <p>A privileged user/process inside guest could use this flaw to crash
    the Qemu process resulting in DoS OR potentially leverage it to
    execute arbitrary code on the host with privileges of the Qemu
    process.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7718">CVE-2017-7718</a>

     <p>qemu-kvm built with the Cirrus CLGD 54xx VGA Emulator support is
     vulnerable to an out-of-bounds access issue. It could occur while
     copying VGA data via bitblt functions cirrus_bitblt_rop_fwd_transp_
     and/or cirrus_bitblt_rop_fwd_.</p>

     <p>A privileged user inside guest could use this flaw to crash the
     Qemu process resulting in DoS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7980">CVE-2017-7980</a>

     <p>qemu-kvm built with the Cirrus CLGD 54xx VGA Emulator support is
     vulnerable to an out-of-bounds r/w access issues. It could occur
     while copying VGA data via various bitblt functions.</p>

     <p>A privileged user inside guest could use this flaw to crash the
     Qemu process resulting in DoS OR potentially execute arbitrary code
     on a host with privileges of Qemu process on the host.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9602">CVE-2016-9602</a>

  <p>Quick Emulator(Qemu) built with the VirtFS, host directory sharing via
  Plan 9 File System(9pfs) support, is vulnerable to an improper link
  following issue.  It could occur while accessing symbolic link files
  on a shared host directory.</p>

  <p>A privileged user inside guest could use this flaw to access host file
  system beyond the shared folder and potentially escalating their
  privileges on a host.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7377">CVE-2017-7377</a>

  <p>Quick Emulator(Qemu) built with the virtio-9p back-end support is
  vulnerable to a memory leakage issue. It could occur while doing a I/O
  operation via v9fs_create/v9fs_lcreate routine.</p>

  <p>A privileged user/process inside guest could use this flaw to leak
  host memory resulting in Dos.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7471">CVE-2017-7471</a>

  <p>Quick Emulator(Qemu) built with the VirtFS, host directory sharing via
  Plan 9 File System(9pfs) support, is vulnerable to an improper access
  control issue.  It could occur while accessing files on a shared host
  directory.</p>

  <p>A privileged user inside guest could use this flaw to access host file
  system beyond the shared folder and potentially escalating their
  privileges on a host.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7493">CVE-2017-7493</a>

  <p>Quick Emulator(Qemu) built with the VirtFS, host directory sharing via
  Plan 9 File System(9pfs) support, is vulnerable to an improper access
  control issue.  It could occur while accessing virtfs metadata files
  in mapped-file security mode.</p>

  <p>A guest user could use this flaw to escalate their privileges inside
  guest.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-8086">CVE-2017-8086</a>

  <p>Quick Emulator(Qemu) built with the virtio-9p back-end support is
  vulnerable to a memory leakage issue. It could occur while querying
  file system extended attributes via 9pfs_list_xattr() routine.</p>

  <p>A privileged user/process inside guest could use this flaw to leak
  host memory resulting in Dos.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.1.2+dfsg-6+deb7u22.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1035.data"
# $Id: $
