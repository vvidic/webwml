<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was found in how WPA code can be triggered to
reconfigure WPA/WPA2/RSN keys (TK, GTK, or IGTK) by replaying a specific
frame that is used to manage the keys. Such reinstallation of the
encryption key can result in two different types of vulnerabilities:
disabling replay protection and significantly reducing the security of
encryption to the point of allowing frames to be decrypted or some parts
of the keys to be determined by an attacker depending on which cipher is
used.</p>

<p>Those issues are commonly known under the <q>KRACK</q> appelation. According
to US-CERT, "the impact of exploiting these vulnerabilities includes
decryption, packet replay, TCP connection hijacking, HTTP content
injection, and others."</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13077">CVE-2017-13077</a>

    <p>Reinstallation of the pairwise encryption key (PTK-TK) in the
    4-way handshake.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13078">CVE-2017-13078</a>

    <p>Reinstallation of the group key (GTK) in the 4-way handshake.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13079">CVE-2017-13079</a>

    <p>Reinstallation of the integrity group key (IGTK) in the 4-way
    handshake.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13080">CVE-2017-13080</a>

    <p>Reinstallation of the group key (GTK) in the group key handshake.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13081">CVE-2017-13081</a>

    <p>Reinstallation of the integrity group key (IGTK) in the group key
    handshake.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13082">CVE-2017-13082</a>

    <p>Accepting a retransmitted Fast BSS Transition (FT) Reassociation
    Request and reinstalling the pairwise encryption key (PTK-TK)
    while processing it.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13084">CVE-2017-13084</a>

    <p>Reinstallation of the STK key in the PeerKey handshake.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13086">CVE-2017-13086</a>

    <p>reinstallation of the Tunneled Direct-Link Setup (TDLS) PeerKey
    (TPK) key in the TDLS handshake.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13087">CVE-2017-13087</a>

    <p>reinstallation of the group key (GTK) when processing a Wireless
    Network Management (WNM) Sleep Mode Response frame.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13088">CVE-2017-13088</a>

    <p>reinstallation of the integrity group key (IGTK) when processing a
    Wireless Network Management (WNM) Sleep Mode Response frame.</p>
    
</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.0-3+deb7u5. Note that the latter two vulnerabilities (<a href="https://security-tracker.debian.org/tracker/CVE-2017-13087">CVE-2017-13087</a>
and <a href="https://security-tracker.debian.org/tracker/CVE-2017-13088">CVE-2017-13088</a>) were mistakenly marked as fixed in the changelog
whereas they simply did not apply to the 1.0 version of the WPA source
code, which doesn't implement WNM sleep mode responses.</p>

<p>We recommend that you upgrade your wpa packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1150.data"
# $Id: $
