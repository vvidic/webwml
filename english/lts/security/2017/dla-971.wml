<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7502">CVE-2017-7502</a>

  <p>A null pointer dereference vulnerability in NSS was found when server
  receives empty SSLv2 messages. This issue was introduced with the recent
  removal of SSLv2 protocol from upstream code in 3.24.0 and introduction
  of dedicated parser able to handle just sslv2-style hello messages.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, this problem has been fixed in version
2:3.26-1+debu7u4.</p>

<p>We recommend that you upgrade your nss packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-971.data"
# $Id: $
