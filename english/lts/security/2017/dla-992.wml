<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The Qualys Research Labs discovered various problems in the dynamic
linker of the GNU C Library which allow local privilege escalation by
clashing the stack. For the full details, please refer to their advisory
published at:
<a href="https://www.qualys.com/2017/06/19/stack-clash/stack-clash.txt">https://www.qualys.com/2017/06/19/stack-clash/stack-clash.txt</a></p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.13-38+deb7u12.</p>

<p>We recommend that you upgrade your eglibc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-992.data"
# $Id: $
