<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities have been discovered in the Apache HTTP server:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44224">CVE-2021-44224</a>

    <p>When operating as a forward proxy, Apache was depending on the setup
    suspectable to denial of service or Server Side Request forgery.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44790">CVE-2021-44790</a>

    <p>A buffer overflow in mod_lua may result in denial of service or potentially
    the execution of arbitrary code.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.4.25-3+deb9u12.</p>

<p>We recommend that you upgrade your apache2 packages.</p>

<p>For the detailed security status of apache2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/apache2">https://security-tracker.debian.org/tracker/apache2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2907.data"
# $Id: $
