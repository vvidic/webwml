<define-tag description>Debian 9 Long Term Support reaching end-of-life</define-tag>
<define-tag moreinfo></p>
<p>
The Debian Long Term Support (LTS) Team hereby announces that Debian 9
stretch support has reached its end-of-life on July 1, 2022,
five years after its initial release on June 17, 2017.
</p>
<p>
Debian will not provide further security updates for Debian 9. A
subset of stretch packages will be supported by external parties.
Detailed information can be found at <a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a>.
</p>
<p>
The LTS Team will prepare the transition to Debian 10 buster, which is the
current oldstable release. The LTS team will take over support from the
Security Team during August, while the final point update for buster will
be released during that month.
</p>
<p>
Debian 10 will also receive Long Term Support for five years after its
initial release with support ending on June 30, 2024. The supported
architectures will be announced at a later date.
</p>
<p>
For further information about using buster and upgrading from stretch
LTS, please refer to <a href="https://wiki.debian.org/LTS/Using">LTS/Using</a>.
</p>
<p>
Debian and its LTS Team would like to thank all contributing users,
developers and sponsors who are making it possible to extend the life
of previous stable releases, and who have made this LTS a success.
</p>
<p>
If you rely on Debian LTS, please consider <a href="https://wiki.debian.org/LTS/Development">joining the team</a>,
providing patches, testing or <a href="https://wiki.debian.org/LTS/Funding">funding the efforts</a>.
</p>
<p>
More information about Debian Long Term Support can be found at
<a href="https://wiki.debian.org/LTS/">the LTS wiki page</a>.
</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3067.data"
# $Id: $
