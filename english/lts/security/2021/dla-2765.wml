<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple issues have been discovered in mupdf.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10246">CVE-2016-10246</a>

    <p>Buffer overflow in the main function in jstest_main.c allows remote attackers
    to cause a denial of service (out-of-bounds write) via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10247">CVE-2016-10247</a>

    <p>Buffer overflow in the my_getline function in jstest_main.c allows remote attackers
    to cause a denial of service (out-of-bounds write) via a crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6060">CVE-2017-6060</a>

    <p>Stack-based buffer overflow in jstest_main.c allows remote attackers
    to have unspecified impact via a crafted image.</p>

<p></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10289">CVE-2018-10289</a>

    <p>An infinite loop in the fz_skip_space function of the pdf/pdf-xref.c file.
    A remote adversary could leverage this vulnerability to cause a denial of
    service via a crafted pdf file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000036">CVE-2018-1000036</a>

    <p>Multiple memory leaks in the PDF parser allow an attacker to cause a denial
    of service (memory leak) via a crafted file.</p>

<p></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-19609">CVE-2020-19609</a>

    <p>A heap based buffer over-write in tiff_expand_colormap() function when parsing TIFF
    files allowing attackers to cause a denial of service.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
1.14.0+ds1-4+deb9u1.</p>

<p>We recommend that you upgrade your mupdf packages.</p>

<p>For the detailed security status of mupdf please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mupdf">https://security-tracker.debian.org/tracker/mupdf</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2765.data"
# $Id: $
