<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in gthumb, an image viewer and browser.
A heap-based buffer overflow in _cairo_image_surface_create_from_jpeg()
in extensions/cairo_io/cairo-image-surface-jpeg.c allows attackers to
cause a crash and potentially execute arbitrary code via a crafted JPEG
file.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
3:3.4.4.1-5+deb9u2.</p>

<p>We recommend that you upgrade your gthumb packages.</p>

<p>For the detailed security status of gthumb please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/gthumb">https://security-tracker.debian.org/tracker/gthumb</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2749.data"
# $Id: $
