<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Multiple vulnerabilities have been discovered in pdns, an authoritative
DNS server. The Common Vulnerabilities and Exposures project identifies
the following problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5426">CVE-2016-5426</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-5427">CVE-2016-5427</a>

    <p>Florian Heinz and Martin Kluge reported that the PowerDNS
    Authoritative Server accepts queries with a qname's length larger
    than 255 bytes and does not properly handle dot inside labels. A
    remote, unauthenticated attacker can take advantage of these flaws
    to cause abnormal load on the PowerDNS backend by sending specially
    crafted DNS queries, potentially leading to a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6172">CVE-2016-6172</a>

    <p>It was reported that a malicious primary DNS server can crash a
    secondary PowerDNS server due to improper restriction of zone size
    limits. This update adds a feature to limit AXFR sizes in response
    to this flaw.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.1-4.1+deb7u2.</p>

<p>We recommend that you upgrade your pdns packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-627.data"
# $Id: $
