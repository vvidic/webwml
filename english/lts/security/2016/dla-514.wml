<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were found in libxslt.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7995">CVE-2015-7995</a>

    <p>A missing type check could cause an application crash via a
    especially crafted file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1683">CVE-2016-1683</a>

    <p>An out of bounds heap access bug was found in libxslt.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1684">CVE-2016-1684</a>

    <p>There was an integer overflow bug in libxslt that could lead to an
    application crash.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.1.26-14.1+deb7u1.</p>

<p>We recommend that you upgrade your libxslt packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-514.data"
# $Id: $
