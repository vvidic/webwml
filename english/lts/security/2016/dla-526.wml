<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability in the MySQL Connectors component of Oracle MySQL
(subcomponent: Connector/J) has been discovered that may result in
unauthorized update, insert or delete access to some MySQL Connectors
accessible data as well as read access to a subset of MySQL Connectors.
The issue is addressed by updating to the latest stable release of
mysql-connector-java since Oracle did not release further information.</p>

<p>Please see Oracle's Critical Patch Update advisory for further details.</p>

<p><url "http://www.oracle.com/technetwork/topics/security/cpuapr2015verbose-2365613.html#MSQL"></p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.1.39-1~deb7u1.</p>

<p>We recommend that you upgrade your mysql-connector-java packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-526.data"
# $Id: $
