<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were fixed in the Wireshark network
protocol analyzer.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10894">CVE-2019-10894</a>

    <p>GSS-API dissector crash</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10895">CVE-2019-10895</a>

    <p>NetScaler file parser crash</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10896">CVE-2019-10896</a>

    <p>DOF dissector crash</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10899">CVE-2019-10899</a>

    <p>SRVLOC dissector crash</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10901">CVE-2019-10901</a>

    <p>LDSS dissector crash</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10903">CVE-2019-10903</a>

    <p>DCERPC SPOOLSS dissector crash</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12295">CVE-2019-12295</a>

    <p>Dissection engine could crash</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.6.8-1.1~deb9u1.</p>

<p>We recommend that you upgrade your wireshark packages.</p>

<p>For the detailed security status of wireshark please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/wireshark">https://security-tracker.debian.org/tracker/wireshark</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2423.data"
# $Id: $
