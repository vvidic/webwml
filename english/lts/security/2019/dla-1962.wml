<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The <q>send_email</q> function in graphite-web/webapp/graphite/composer/views.py
in Graphite is vulnerable to SSRF. The vulnerable SSRF endpoint can be used
by an attacker to have the Graphite web server request any resource.
The response to this SSRF request is encoded into an image file and then sent
to an e-mail address that can be supplied by the attacker. Thus, an attacker
can exfiltrate any information.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.9.12+debian-6+deb8u1.</p>

<p>We recommend that you upgrade your graphite-web packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1962.data"
# $Id: $
