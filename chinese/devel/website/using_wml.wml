#use wml::debian::template title="使用 WML"
#use wml::debian::translation-check translation="24a8bbc5bf2fd2fbe025f0baa536bf1126f83723"

<p>WML（Website META Language）代表网站元语言。<q>元语言</q>一词意味着 WML 接收输入的\
.wml 文件，处理其中的任何内容（可以是从基础 HTML 到 Perl 代码的任何内容！），\
然后输出您想要它输出的任何内容，例如 .html 或 .php 文件。</p>

<p>尽管 WML 文档非常完整且 WML 本身非常强大，但是直到您开始理解它是如何工作的之前，\
从示例中学习是最容易的方式。您可能会发现用于 Debian 站点的模板文件很有用。它们可以在\
<code><a href="https://salsa.debian.org/webmaster-team/webwml/tree/master/english/template/debian">\
webwml/english/template/debian/</a></code>中被找到。</p>

<p>此处假定您在自己的计算机上安装了 WML。可通过
<a href="https://packages.debian.org/wml">Debian 软件包</a>获取 WML。


<h2>编辑 WML 源文件</h2>

<p>所有.wml 文件都具备的一个特点是以一个或多个<code>#use</code>作为开头。您不应该\
更改或翻译其语法，只应更改或翻译带引号的字符串，如在 <code>title=</code>之后的\
字符串，这会改变在输出文件中的 &lt;title&gt; 元素。</p>

<p>除了标题行之外，我们的大多数.wml 页面都包含简单的 HTML。如果您遇到诸如 \
&lt;define-tag&gt; 或 &lt;: ... :&gt; 之类的标签，请小心，因为这些分界码是由\
WML 的特殊处理过程之一进行处理的。请查看下方以获得更多的信息。</p>


<h2>构建 Debian 网页</h2>

<p>简单地在 webwml/&lt;lang&gt; 中输入<kbd> make </kbd>。我们就已经创建了使用所有\
正确参数调用<kbd> wml </kbd>的 makefiles。</p>

<p>如果您执行<kbd> make install </kbd>，那么 HTML 文件就会被构建并存放在\
<kbd>../../www/</kbd>目录中。</p>


<h2>我们使用的 WML 的额外功能</h2>

<p>我们广泛使用的 WML 的功能之一是 Perl 的使用。请记住，我们并没有使用动态页面。\
在生成 HTML 页面时，Perl 作为编程语言被用来实现各种高级功能。\
有两个典型的例子可以展示 Perl 在页面中的用途：一是将其用于在网站主页\
创建最新新闻列表，二是在页面末尾生成指向翻译页面的链接。</p>

# TODO: add the basic stuff from webwml/english/po/README here

<p>要重新构建我们网站的模板，您需要安装 wml 版本 &gt;= 2.0.6。要为非英语语言的翻译文件\
重新构建 gettext 模板， mp4h &gt;= 1.3.0 是必需的。</p>


<h2>WML 的特定问题</h2>

<p>多字节语言可能需要对.wml 文件进行特殊的预处理或后处理，以便正确处理字符集。这\
可以通过恰当地改变<kbd> WMLPROLOG </kbd>和在 <kbd>webwml/&lt;lang&gt;/Make.lang</kbd> \
中的<kbd> WMLEPILOG </kbd>变量来完成。根据您的<kbd> WMLEPILOG </kbd>\
的工作方式，您可能需要改变<kbd> WMLOUTFILE </kbd>的值。
<br>
有关示例，请参见日语或中文翻译。
</p>
