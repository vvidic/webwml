#use wml::debian::translation-check translation="7feda280e03c75f0fc6b2380a750bce08b2f9d0d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service ou une
fuite d'informations.</p>

<p>Cette mise à jour n’est pas encore disponible pour l’architecture armhf
(ARM EABI hard-float).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36311">CVE-2020-36311</a>

<p>Un défaut a été découvert dans le sous-système KVM pour les processeurs AMD,
permettant à un attaquant de provoquer un déni de service en déclenchant une
destruction de VM SEV.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3609">CVE-2021-3609</a>

<p>Norbert Slusarek a signalé une vulnérabilité de situation de compétition
dans la gestion de réseau de protocole CAN de BCM, permettant à un attaquant
local d’augmenter ses privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33909">CVE-2021-33909</a>

<p>Qualys Research Labs a découvert une vulnérabilité de conversion
<q>size_t vers int</q> dans la couche système de fichiers du noyau Linux. Un
attaquant local non privilégié capable de créer, monter puis supprimer une
structure profonde de répertoires dont la longueur totale du chemin dépasse
1 Go, peut tirer avantage de ce défaut pour une élévation de privilèges.</p>

<p>Vous trouverez plus de détails dans l'annonce de Qualys à l'adresse
<a href="https://www.qualys.com/2021/07/20/cve-2021-33909/sequoia-local-privilege-escalation-linux.txt">https://www.qualys.com/2021/07/20/cve-2021-33909/sequoia-local-privilege-escalation-linux.txt</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-34693">CVE-2021-34693</a>

<p>Norbert Slusarek a signalé une fuite d’informations dans la gestion de
réseau de protocole CAN de BCM. Un attaquant local peut tirer avantage du
défaut pour obtenir des informations sensibles à partir de la mémoire de pile
du noyau.</p></li>
</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 4.19.194-3~deb9u1. De plus cette annonce corrige une régression dans la
précédente mise à jour (<a href="https://bugs.debian.org/990072">n° 990072</a>)
qui affecte LXC.</p>


<p>Nous vous recommandons de mettre à jour vos paquets linux-4.19.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux-4.19, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux-4.19">\
https://security-tracker.debian.org/tracker/linux-4.19</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2714.data"
# $Id: $
