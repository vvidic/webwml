#use wml::debian::translation-check translation="c73d9ae0a1272c51bfa5a487512aa5d7fa6b2af4" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Tavis Ormandy a découvert que nss, la bibliothèque de service de
sécurité réseau de Mozilla, est prédisposée à un défaut de dépassement de
tas lors de la vérification de signatures DSA ou RSA-PPS, qui pourrait
avoir pour conséquences un déni de service ou éventuellement l'exécution de
code arbitraire.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2:3.26.2-1.1+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nss.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de nss, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/nss">\
https://security-tracker.debian.org/tracker/nss</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2836.data"
# $Id: $
