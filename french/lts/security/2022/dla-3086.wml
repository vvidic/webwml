#use wml::debian::translation-check translation="7a31173ac25cdb448c3d5a8b6ba4ffac88ab73ff" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La classe Commandline dans maven-shared-utils, une collection de diverses
classes d'utilitaire pour le système de construction Maven, peut émettre des
chaînes à guillemets doubles sans les protéger correctement, permettant des
attaques d'injection de commande d'interpréteur.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans la version
3.3.0-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets maven-shared-utils.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de maven-shared-utils,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/maven-shared-utils">\
https://security-tracker.debian.org/tracker/maven-shared-utils</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3086.data"
# $Id: $
