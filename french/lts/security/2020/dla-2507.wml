#use wml::debian::translation-check translation="019d03b3baaee431755b0558412b8ca20da18276" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans XStream, une
bibliothèque Java pour sérialiser des objets vers et depuis XML</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26258">CVE-2020-26258</a>

<p>XStream est vulnérable à une contrefaçon de requête côté serveur pouvant
être activée lors de la désérialisation. Cette vulnérabilité peut permettre à un
attaquant distant de demander des données de ressources internes privées
simplement en manipulant le flux d’entrée traité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26259">CVE-2020-26259</a>

<p>Xstream est vulnérable à une suppression de fichiers arbitraires sur l’hôte
local lors de la désérialisation. Cette vulnérabilité peut permettre à un
attaquant distant de supprimer des fichiers connus arbitraires, aussi longtemps
que le processus en cours ait les droits nécessaires, simplement en manipulant
le flux d’entrée traité.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.4.11.1-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxstream-java.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libxstream-java, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libxstream-java">https://security-tracker.debian.org/tracker/libxstream-java</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2507.data"
# $Id: $
