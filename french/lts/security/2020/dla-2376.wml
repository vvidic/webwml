#use wml::debian::translation-check translation="6f2b1db59300467b187ff13b16c9131297299f8c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans la boîte à outils Qt.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19872">CVE-2018-19872</a>

<P>Une image PPM mal formée provoque un plantage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17507">CVE-2020-17507</a>

<p>Lecture excessive de tampon dans l’analyseur XBM.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 5.7.1+dfsg-3+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qtbase-opensource-src.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de qtbase-opensource-src, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/qtbase-opensource-src">https://security-tracker.debian.org/tracker/qtbase-opensource-src</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2376.data"
# $Id: $
