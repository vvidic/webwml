#use wml::debian::template title="기부"
#use wml::debian::translation-check translation="ff49b18e1e0e4a97067fd8780a8e6e6b7ab8458d" maintainer="Sebul"

<p>기부는
<a href="$(HOME)/devel/leader">데비안 프로젝트 리더 Debian Project Leader</a> (DPL)
가 관리하며 데비안은
<a href="https://db.debian.org/machines.cgi">기계</a>,
<a href="https://wiki.debian.org/Teams/DSA/non-DSA-HW">다른 하드웨어</a>,
도메인, 암호 인증서,
<a href="https://www.debconf.org">데비안 컨퍼런스</a>,
<a href="https://wiki.debian.org/MiniDebConf">데비안 미니 컨퍼런스</a>,
<a href="https://wiki.debian.org/Sprints">개발 스프린트</a>,
다른 이벤트 및 기타 사항을 제공합니다.
데비안을 지원하는 <a href="#donors">기부자들</a> 고맙습니다!
</p>

<p id="default">가장 쉬운 데비안 기부 방법은 데비안의 자산을 신뢰하는 비영리단체 
<a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest</a>에 PayPal 하는 것 입니다.
여러분은 <a href="#methods">아래 방법</a>으로 기부할 수 있습니다.
<!--
You can donate via the <a href="#methods">methods listed below</a>.
-->
</p>

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<div>
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="86AHSZNRR29UU">
<input type="submit" value="Donate via PayPal" />
</div>
</form>

<!--
Hidden due to an issue with USAePay/Paysimple
https://lists.debian.org/msgid-search/CABJgS1ZtpcfM3uzA+j9_mp6ocR2H=p4cOcWWs2rjSNwhWYWepw@mail.gmail.com
<form method="post" action="https://www.usaepay.com/interface/epayform/">
<div>
  <span>$<input type="text" name="UMamount" size="6"> USD, paid once only</span>
  <input name="UMdescription" value="Debian general contribution" type="hidden">
  <input name="UMkey" value="ijWB0O98meGpX0LrCP0eb1Y94sI1Dl67" type="hidden">
  <input name="UMcommand" value="sale" type="hidden" />
  <input type="submit" tabindex="2" value="Donate" />
</div>
</form>
-->

<h2 id="methods">기부 방법</h2>

<p>여러
<a href="https://wiki.debian.org/Teams/Treasurer/Organizations">기관</a>이
데비안 자산을 신뢰하고 데비안 대신 기부를 받습니다.
</p>

<table>
<tr>
<th>기관</th>
<th>방법</th>
<th>노트</th>
</tr>
<tr>
<td><a href="#spi"><acronym title="Software in the Public Interest">SPI</acronym></a></td>
<td>
 <a href="#spi-paypal">PayPal</a>,
<!--
Hidden due to an issue with USAePay/Paysimple
https://lists.debian.org/msgid-search/CABJgS1ZtpcfM3uzA+j9_mp6ocR2H=p4cOcWWs2rjSNwhWYWepw@mail.gmail.com
 <a href="#spi-usa-epay">USA ePay</a>,
-->
 <a href="#spi-click-n-pledge">클릭 &amp; 서약</a> (반복 기부),
 <a href="#spi-cheque">수표</a> (USD/CAD),
 <a href="#spi-other">기타</a>
</td>
<td>미국, 면세 비영리</td>
</tr>
<tr>
<td><a href="#debianfrance">데비안 프랑스</a></td>
<td>
 <a href="#debianfrance-bank">전신 송금</a>,
 <a href="#debianfrance-paypal">PayPal</a>
</td>
<td>프랑스, 면세 비영리</td>
</tr>
<tr>
<td><a href="#debianch">debian.ch</a></td>
<td>
 <a href="#debianch-bank">전신 송금</a>,
 <a href="#debianch-other">기트</a>
</td>
<td>스위스, 비영리</td>
</tr>
<tr>
<td><a href="#debian">데비안</a></td>
<td>
 <a href="#debian-equipment">장비</a>,
 <a href="#debian-time">시간</a>,
 <a href="#debian-other">기타</a>
</td>
<td></td>
</tr>

# Template:
#<tr>
#<td><a href="#"><acronym title=""></acronym></a></td>
#<td>
# <a href="#"></a>,
# <a href="#"></a> (allows recurring donations),
# <a href="#cheque"></a> (CUR)
#</td>
#<td>, tax-exempt non-profit</td>
#</tr>

</table>

<h3 id="spi">공익 소프트웨어</h3>

<p>
<a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest, Inc.</a>는
미국에 기반한 면세 비영리 기관이며, 데비안 사람들이 1997년에 자유 소프트웨어/하드웨어 기관을
돕기 위해 만들었습니다.
</p>

<h4 id="spi-paypal">PayPal</h4>
<p>
단일 및 반복 기부를 PayPal 웹사이트
<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">SPI 페이지</a>에서
할 수 있습니다.
반복 기부 하려면, "Make This Recurring (Monthly)" 상자를 체크하세요.
</p>

<h4 id="spi-click-n-pledge">클릭 &amp; 서약</h4>

<p>
단일 및 반부 기부를 Click &amp; Pledge 웹사이트에서
<a href="https://co.clickandpledge.com/advanced/default.aspx?wid=34115">SPI 
페이지</a>에서 할 수 있습니다.
반복 기부 하려면,
오른 쪽에서 빈도를 택하고,
"Debian Project Donation"까지 스크롤하고,
기부할 금액을 입력,"Add to cart" 항목 클릭하고 나머지 과장을 진행하세요.
</p>

<!--
Unfortunately this isn't possible to do yet:
<form method="post" action="https://co.clickandpledge.com/advanced/default.aspx?wid=34115">
<input name="ScriptManager1" value="UpdatePanel1|btnDAddToCart_dnt_33979" type="hidden">
$<input name="DAmount_33979" size="6" id="DAmount_33979" type="text"> USD, paid
<select name="cboxRecurring" id="cboxRecurring">
  <option value="-1">once only</option>
  <option value="0">weekly</option>
  <option value="1">fortnightly</option>
  <option value="2">monthly</option>
  <option value="3">every 2 months</option>
  <option value="4">quarterly</option>
  <option value="5">bi-annually</option>
  <option value="6">annually</option>
</select>
<input type="submit" name="btnDAddToCart_dnt_33977" id="btnDAddToCart_dnt_33977" value="Donate"/>
</form>
-->

<h4 id="spi-cheque">수표</h4>

<p>
기부는 수표 또는 머니 오더를 통해
<abbr title="US dollars">USD</abbr> 및
<abbr title="Canadian dollars">CAD</abbr>로 할 수 있습니다.
memo 필드에 데비안을 넣고 <a href="https://www.spi-inc.org/donations/">SPI 기부 페이지</a> 목록에 있는 SPI에 보내세요.
</p>

<h4 id="spi-other">기타</h4>

<p>
전신 송금 및 기타 방법을 통한 기부도 가능합니다.
세계의 일부 지역에서는 공공 이익을 위해 소프트웨어 파트너 조직 중 하나에 기부하는 것이 더 쉬울 수 있습니다. 자세한 내용은 <a href="https://www.spi-inc.org/donations/">SPI 
기부 페이지</a>를 방문 하십시오.</p>

<h3 id="debianfrance">데비안 프랑스</h3>

<p>
<a href="https://france.debian.net/">데비안 프랑스 협회</a>는
프랑스 데비안 프로젝트를 지원하고 증진시키기 위해 만들어져 <q>law of 1901</q> 아래에 프랑스에서 등록된 기관입니다.
</p>

<h4 id="debianfrance-bank">전신 송금</h4>
<p>
전신 송금을 통한 기부는 <a href="https://france.debian.net/soutenir/#compte">데비안 프랑스 기부 페이지</a>에 나와있는 은행 계좌에서 가능합니다. <a href="mailto:donation@france.debian.net">donation@france.debian.net</a>으로 이메일을 보내면 기부금 영수증을 받을 수 있습니다.
</p>

<h4 id="debianfrance-paypal">PayPal</h4>
<p>
기부금을
<a href="https://france.debian.net/galette/plugins/paypal/form">Debian France PayPal 페이지</a>로 보낼 수 있습니다.
특히 데비안 프랑스 또는 데비안 프로젝트로 향할 수 있습니다.
</p>

<h3 id="debianch">debian.ch</h3>

<p>
<a href="https://debian.ch/">debian.ch</a>를
스위스와 리히텐슈타인 공국의 데비안 프로젝트를 대표하기 위해 설립했습니다.
</p>

<h4 id="debianch-bank">전신 송금</h4>

<p>
스위스 은행과 국제 은행의 계좌 이체를 통한 기부는
<a href="https://debian.ch/">debian.ch 웹사이트</a>에 있는 은행 계좌에서 가능합니다.

</p>

<h4 id="debianch-other">기타</h4>

<p>
다른 방법을 통한 기부는 <a href="https://debian.ch/">debian.ch 웹사이트</a>에 나와있는 기부 주소로 연락하세요.</p>

# Template:
#<h3 id=""></h3>
#
#<p>
#</p>
#
#<h4 id=""></h4>
#
#<p>
#</p>

<h3 id="debian">데비안</h3>

<p>데비안은 직접
<a href="#debian-equipment">장비</a> 기증을 받을 수 있지만
<a href="#debian-other">다른</a> 기부는 아닙니다.
</p>

<a name="equipment_donations"></a>
<h4 id="debian-equipment">장치와 서비스</h4>

<p>
데비안은 또한 개인, 회사, 대학 등으로부터 장비와 서비스를 기부하여 데비안이 세계와 연결되도록합니다.
</p>

<p>
회사에 쉬는 시스템이나 예비 장비(하드 드라이브, SCSI 컨트롤러, 네트워크 카드 등)가 있으면 데비안에 기부하십시오. 자세한 내용은 <a href="mailto:hardware-donations@debian.org">하드웨어 기부 담당자</a>에게 문의 하십시오.
</p>

<p>데비안은 프로젝트 내의 다양한 서비스 및 그룹에 <a href="https://wiki.debian.org/Hardware/Wanted">필요한 하드웨어 목록</a>을 유지 관리합니다.
</p>

<h4 id="debian-time">시간</h4>

<p>여러분의 개인적 또는 일하는 시간을 사용하는
<a href="$(HOME)/intro/help">데비안을 돕는</a> 여러 방법이 있습니다.
</p>

<h4 id="debian-other">기타</h4>

<p>
데비안은 지금은 cryptocurrencies를 받아들일 수 없지만 
이 기부 방법을 지원할 수있는 방법을 찾고 있습니다.
# Brian Gupta requested we discuss this before including it:
#If you have cryptocurrency to donate, or insights to share, please
#get in touch with <a href="mailto:madduck@debian.org">Martin f. krafft</a>.
</p>

<h2 id="donors">기부자</h2>

<p>다음은 데비안에 장비 또는 서비스를 기부한 기관 목록입니다.:</p>

<ul>
  <li><a href="https://db.debian.org/machines.cgi">호스팅 및 하드웨어 후원자</a></li>
  <li><a href="mirror/sponsors">미러 후원자</a></li>
  <li><a href="partners/">개발 및 서비스 파트너</a></li>
</ul>
