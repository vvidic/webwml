#use wml::debian::template title="Debian Pure Blends" BARETITLE="true"
#use wml::debian::translation-check translation="6d2e0afb56e760364713c2cca2c9f6407c9a744f"
# Translator: Fabian Baumanis <fabian.baumanis@mailbox.org>, 2020.

<p>
Debian Pure Blends sind Lösungen für Nutzergruppen mit spezifischen Anforderungen.
Sie beinhalten nicht nur praktische Sammlungen spezifischer Pakete (Meta-Pakete), sondern
vereinfachen die Installation und die Konfiguration für den bestimmten Einsatzbereich.
Sie umfassen die Interessen verschiedenster Personen, egal ob Kinder, Wissenschaftler,
Gamer, Anwälte, medizinisches Personal, beeinträchtigte Personen, etc.
Das gemeinsame Ziel aller Blends ist die Vereinfachung der Installation und Administration von Computern
für bestimmte Personengruppen. Des Weiteren soll auch eine Verbindung zwischen diesen Zielgruppen und den
Entwicklern, die die Software schreiben und paketieren, hergestellt werden.
</p>

<p>
Sie können mehr über Debian Pure Blends im <a href="https://blends.debian.org/blends/">Pure Blends Handbuch</a> nachlesen.
</p>

<ul class="toc">
<li><a href="#released">Veröffentlichte Pure Blends</a></li>
<li><a href="#unreleased">Pure Blends in Entwicklung</a></li>
<li><a href="#development">Entwicklung</a></li>
</ul>

<div class="card" id="released">
<h2>Veröffentlichte Pure Blends</h2>
<div>
<p>Das Wort "veröffentlicht" kann verschiedene Bedeutungen für Pure Blends haben. Meistens beinhaltet das Blend Meta-Pakete
oder einen Installer, der mit einer Stable-Veröffentlichung von Debian ausgeliefert wurde. Blends können außerdem Installationsmedien
bereitstellen oder die Basis für eine Distribution bilden. Für nähere Informationen zu bestimmten Blends konsultieren Sie bitte
die Seite des jeweiligen Blends.</p>

<table class="tabular" summary="">
<tbody>
 <tr>
  <th>Blend</th>
  <th>Beschreibung</th>
  <th>Links</th>
 </tr>
#include "../../english/blends/released.data"
</tbody>
</table>
</div>
</div>

<div class="card" id="unreleased">
  <h2>Pure Blends in Entwicklung</h2>
  <div>
   <p>Diese Blends befinden sich zurzeit in Entwicklung und haben bisher noch kein Stable Release veröffentlicht.
   Sie können aber in der <a
href="https://www.debian.org/releases/testing/">Testing-</a> oder <a
href="https://www.debian.org/releases/unstable">Unstable-</a> Distribution verfügbar sein. Einige
davon verwenden nicht-freie Komponenten.</p>

<table class="tabular" summary="">
<tbody>
 <tr>
  <th>Blend</th>
  <th>Beschreibung</th>
  <th>Links</th>
 </tr>
#include "../../english/blends/unreleased.data"
</tbody>
</table>
  </div>
 </div>

  <div class="card" id="development">
  <h2>Entwicklung</h2>
  <div>
   <p>Falls Sie bei der Entwicklung von Pure Blends mitwirken möchten, können sie Informationen dazu auf den dazugehörigen <a
href="https://wiki.debian.org/DebianPureBlends">Wiki-Seiten</a> finden.
   </p>
  </div>
</div><!-- #main -->
