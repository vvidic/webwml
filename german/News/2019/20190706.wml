<define-tag pagetitle>Debian 10 <q>Buster</q> veröffentlicht</define-tag>
<define-tag release_date>2019-07-06</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="cbfcd50b343ab08499a29ae770a5528eda687986" maintainer="Erik Pfannenstein"

<p>
Nach fünfundzwanzigmonatiger Entwicklungszeit darf das Debian-Projekt nun 
stolz die neue Stable-Version 10 (Codename <q>Buster</q>) präsentieren, 
die dank der Zusammenarbeit des 
<a href="https://security-team.debian.org/">Debian Sicherheitsteams</a> 
und des
<a href="https://wiki.debian.org/LTS">Debian Long Term Support</a>-Teams über 
die nächsten fünf Jahre Unterstützung erhält.
</p>

<p>
Debian 10 <q>Buster</q> liefert mehrere Desktop-Anwendungen und -Oberflächen 
mit. Unter anderem sind jetzt diese Desktop-Umgebungen enthalten:
</p>
<ul>
<li>Cinnamon 3.8,</li>
<li>GNOME 3.30,</li>
<li>KDE Plasma 5.14,</li>
<li>LXDE 0.99.2,</li>
<li>LXQt 0.14,</li>
<li>MATE 1.20,</li>
<li>Xfce 4.12.</li>
</ul>

<p>
In dieser Veröffentlichung verwendet GNOME anstelle von Xorg standardmäßig den 
Wayland-Display-Server. Wayland hat einen einfacheren und moderneren Aufbau, 
was sicherheitstechnische Vorteile bietet. Allerdings ist der 
Xorg-Display-Server weiterhin vorinstalliert und kann vor dem Start der 
Sitzung ausgewählt werden.
</p>

<p>
Dank des Reproducable-Builds-Projektes ergeben 91% der Quellpakete beim 
Selberkompilieren auf das Bit identische Binärpakete.
Dieses ist ein wichtiges Verifizierungsmerkmal, das Anwenderinnen und Anwender 
vor Sabotagen an Compilern und Build-Netzwerken schützt. Die künftigen 
Debian-Veröffentlichungen werden Werkzeuge und Metadaten enthalten, die es 
Endnutzern ermöglichen, die Herkunft von Paketen innerhalb des Archivs zu 
überprüfen.
</p>

<p>
Für sicherheitskritische Umgebungen ist jetzt AppArmor, ein Zugangskontroll-
Framework, das den Aktionsradius von Programmen einschränkt, standardmäßig 
installiert und eingeschaltet. Desweiteren können alle APT-Methoden 
(ausgenommen cdrom, gpgv und rsh) auf Wunsch <q>seccomp-BPF</q>-Sandboxing 
verwenden. Die https-Methode von APT ist im APT-Paket enthalten und muss nicht 
nachinstalliert werden.</p>

<p>
Die Netzwerk-Filterung setzt in Debian 10 <q>Buster</q> standardmäßig auf das 
nftables-Framework auf. Beginnend bei iptables v1.8.2 enthält das Binärpaket 
iptables-nft und iptables-legacy, das sind zwei Varianten der iptables-
Befehlszeilenschnittstelle, wobei die nftables-basierte Variante Linux' 
nf_tables-Kernel-Subsystem nutzt. Welche der beiden Möglichkeiten zum Einsatz 
kommen soll, lässt sich mit dem <q>alternatives</q>-System ändern.
</p>

<p>
Die UEFI-Unterstützung (<q>Unified Extensible Firmware Interface</q>), die in 
Debian 7 (Codename <q>Wheezy</q>) eingeführt wurde, ist auch für Debian 10 
<q>Buster</q> stark weiterentwickelt worden. Diese Veröffentlichung 
unterstützt jetzt den <q>Sicheren Start</q> auf den Architekturen amd64, i386 
und arm64 und sollte auf den meisten Secure-Boot-geschützten Geräten ohne 
weitere Eingriffe funktionieren. Dies bedeutet, dass man die 
Secure-Boot-Unterstützung in der Firmware-Konfiguration eingeschaltet lassen 
kann.
</p>

<p>
Die Pakete cups und cups-filter sind in Debian 10 <q>Buster</q> standardmäßig 
installiert, sodass alles Nötige vorhanden ist, um treiberloses Drucken nutzen 
zu können. Netzwerk-Druckwarteschlangen und IPP-Drucker werden automatisch 
von cups-browsed eingerichtet und verwaltet, auf unfreie Herstellertreiber 
kann verzichtet werden.
</p>

<p>
Debian 10 <q>Buster</q> enthält eine Vielzahl aktualisierter Softwarepakete 
(über 62% aller Pakete der Vorversion), darunter:</p>

<ul>
<li>Apache 2.4.38</li>
<li>BIND DNS-Server 9.11</li>
<li>Chromium 73.0</li>
<li>Emacs 26.1</li>
<li>Firefox 60.7 (im Paket firefox-esr)</li>
<li>GIMP 2.10.8</li>
<li>GNU Compiler Collection 7.4 und 8.3</li>
<li>GnuPG 2.2</li>
<li>Golang 1.11</li>
<li>Inkscape 0.92.4</li>
<li>LibreOffice 6.1</li>
<li>Linux 4.19 series</li>
<li>MariaDB 10.3</li>
<li>OpenJDK 11</li>
<li>Perl 5.28</li>
<li>PHP 7.3</li>
<li>PostgreSQL 11</li>
<li>Python 3 3.7.2</li>
<li>Ruby 2.5.1</li>
<li>Rustc 1.34</li>
<li>Samba 4.9</li>
<li>systemd 241</li>
<li>Thunderbird 60.7.2</li>
<li>Vim 8.1</li>
<li>mehr als 59.000 andere gebrauchsfertige Softwarepakete, die aus 
fast 29.000 Quellpaketen erzeugt wurden.</li>
</ul>

<p>
Mit dieser breiten Auswahl an Paketen und der traditionell umfrangreichen 
Architekturunterstützung bleibt Debian wieder einmal seinem Ziel treu, das 
universelle Betriebssystem zu sein. Es eignet sich für viele verschiedene 
Anwendungsfälle: vom Arbeitsplatz-PC zum Netbook, vom Entwicklungsserver 
zum Cluster-System sowie für Datenbank-, Web- und Storage-Server. Gleichzeitig 
stellen zusätzliche Qualitätssicherungsmaßnahmen wie automatische 
Installations- und Upgrade-Tests für alle Pakete in Debians Archiv sicher, 
dass <q>Buster</q> die hohen Erwartungen der Leute an eine stabile 
Debian-Veröffentlichung erfüllt.
</p>

<p>
Insgesamt werden zehn Architekturen unterstützt:
64-bit PC/Intel EM64T/x86-64 (<code>amd64</code>),
32-bit PC/Intel IA-32 (<code>i386</code>),
64-bit little-endian Motorola/IBM PowerPC (<code>ppc64el</code>),
64-bit IBM S/390 (<code>s390x</code>),
für ARM <code>armel</code>
und <code>armhf</code> für ältere und neuere 32-Bit-Hardware, 
außerdem <code>arm64</code> für die 64-bittige <q>AArch64</q>-Architektur,
für MIPS <code>mips</code> (big-endian), 
<code>mipsel</code>-(little-endian)-Architekturen für 32-Bit-Hardware 
und <code>mips64el</code> für 64-Bit-Little-Endian-Hardware.
</p>

<h3>Neugierig geworden?</h3>
<p>
Wenn Sie Debian 10 <q>buster</q> nur ausprobieren wollen, ohne es gleich 
zu installieren, können Sie eins der 
<a href="$(HOME)/CD/live/">Live-Abbbilder</a> verwenden, welche das gesamte 
Betriebssystem in einem nur-lesbaren Zustand in den Speicher Ihres Computers 
laden.
</p>

<p>
Diese Live-Abbilder werden für die <code>amd64-</code>
<code>i386-</code>Architekturen angeboten und sind für DVDs, USB-Sticks 
und Netboot-Setups erhältlich. Man kann zwischen verschiedenen 
Desktop-Umgebungen wählen: Cinnamon, GNOME, KDE Plasma, LXDE, MATE, Xfce 
und, neu in Buster, LXQt. Mit Debian Buster wird ein Live-Abbild wieder
neu aufgelegt, das bereits von früher her bekannt ist: das Live-Standard-Image.
Mit diesem kann man ein Debian-Basissystem ohne grafische Benutzeroberfläche testen.
</p>

<p>
Sollte Ihnen das Betriebssystem gefallen, können Sie das Live-Abbild auch für 
die Installation auf Ihrem Computer verwenden. Es enthält sowohl den 
unabhängigen Calamares-Installer als auch den Standard-Debian-Installer.
 Weitere Informationen finden Sie in den 
<a href="$(HOME)/releases/buster/releasenotes">Veröffentlichungshinweisen</a> 
und der Debian-Website unter 
<a href="$(HOME)/CD/live/">Live-Installations-Images.</a>
</p>


<p>
Für die Direkt-Installation von <q>Buster</q> haben Sie die Wahl zwischen 
vielen Installationsmedien wie Blu-Ray-Disc, DVD, CD, USB-Stick und Netzwerk. 
Mit diesen Abbildern können mehrere Desktop-Umgebungen – Cinnamon, GNOME, KDE 
Plasma Desktop und -Anwendungen, LXDE, LXQt, MATE und Xfce – installiert 
werden.
Außerdem gibt es <q>Multi-Architektur</q>-CDs, welche die Installation 
mehrerer Architekturen mit einem einzigen Datenträger ermöglichen. Sie können 
aber genausogut startfähige USB-Instalationsmedien erstellen 
(siehe die <a href="$(HOME)/releases/buster/installmanual">Installationsanleitung</a>
für weitere Details).
</p>

<p>
Für Cloud-Anwender und -Anwenderinnen offeriert Debian direkte Unterstützung für 
die gängigsten Cloud-Plattformen, in deren Abbild-Marktplätzen offizielle 
Debian-Abbilder zur Verfügung stehen. Außerdem veröffentlicht Debian 
<a href="https://cloud.debian.org/images/openstack/current/">vorgefertigte OpenStack-Abbilder</a> 
für die Architekturen <code>amd64</code> und <code>arm64</code> zum 
Herunterladen und Verwenden in eigenen Cloud-Instanzen.
</p>

<p>
Debian kann jetzt in 76 Sprachen installiert werden, von denen die meisten 
sowohl auf den textbasierten als auch auf den grafischen Oberflächen zur 
Verfügung stehen.</p>

<p>
Die Installationsabbilder können ab sofort via
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (empfohlen),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> oder
<a href="$(HOME)/CD/http-ftp/">HTTP</a> heruntergeladen werden; siehe
<a href="$(HOME)/CD/">Debian auf CDs</a> für weitere Informationen. 
<q>Buster</q> wird bei zahlreichen <a href="$(HOME)/CD/vendors">Anbietern</a> bald auch auf physikalischen DVDs, CD-ROMs und Blu-ray Discs erhältlich sein.
</p>

<h3>Debian-Upgrades</h3>
<p>
Upgrades von der vorhergehenden Veröffentlichung Debian 9 
(Codename <q>Stretch</q>) werden für die meisten Konfigurationen von der 
APT-Paketverwaltung automatisch bewältigt.
Wie immer können Debian-Systeme schmerzfrei, auf der Stelle und ohne 
Ausfallzeit aktualisiert werden, aber es wird dringend empfohlen, die 
<a href="$(HOME)/releases/buster/releasenotes">Veröffentlichungshinweise</a> 
sowie die <a href="$(HOME)/releases/buster/installmanual">Installationsanleitung</a> 
für mögliche Probleme und detaillierte Anweisungen zum Installieren und Upgraden 
zu Rate zu ziehen. Die Veröffentlichungshinweise werden in den kommenden 
Wochen weiterhin nachgepflegt und in zusätzliche Sprachen übersetzt werden.
</p>


<h2>Über Debian</h2>

<p>
Debian ist ein freies Betriebssystem, welches von tausenden Freiwilligen 
entwickelt wird, welche sich über das Internet zusammentun. Die 
grundlegenden Stärken des Debian-Projekts sind seine freiwillige Basis, sein 
Beruhen auf dem Debian-Sozialvertrag und Freier Software und sein 
Engagement, das beste aller Betriebssyteme hervorzubringen. Diese neue 
Veröffentlichung ist ein weiterer Schritt in diese Richtung.</p>


<h2>Kontaktinformationen</h2>

<p>
Für weitere Informatioenn besuchen Sie bitte die Debian-Website unter 
<a href="$(HOME)/">https://www.debian.org/</a> oder senden eine E-Mail 
(auf Englisch) an &lt;press@debian.org&gt;.
</p>
